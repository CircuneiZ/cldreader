#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

import console
import sys
import codecs

if len(sys.argv) < 2:
    print('Usage: %s <Zandronum_CLD_file>' % (sys.argv[0]))
    exit(1)

if sys.stdout.encoding != 'cp866':
    sys.stdout = codecs.getwriter('cp866')(sys.stdout.buffer, 'strict')
if sys.stderr.encoding != 'cp866':
    sys.stderr = codecs.getwriter('cp866')(sys.stderr.buffer, 'strict')

import cldproc
import cldenum # for different purposes
from cldenum import *
import struct

File = open(sys.argv[1], 'rb')

def MAKE_ID(lc4):
    s = struct.unpack('<l', lc4.encode('latin1')[0:4])[0]
    return s

def NETWORK_ReadLong():
    return struct.unpack('<l', File.read(4))[0]

def NETWORK_ReadShort():
    return struct.unpack('<h', File.read(2))[0]

def NETWORK_ReadByte():
    return struct.unpack('<b', File.read(1))[0]

def NETWORK_ReadULong():
    return struct.unpack('<L', File.read(4))[0]

def NETWORK_ReadUShort():
    return struct.unpack('<H', File.read(2))[0]

def NETWORK_ReadUByte():
    return struct.unpack('<B', File.read(1))[0]

def NETWORK_ReadFloat():
    return struct.unpack('<f', File.read(4))[0]

def NETWORK_ReadString():
    _s = ''
    while True:
        c = File.read(1).decode('latin1')
        #print(repr(c))
        if not c:
            break
        if c == '\0':
            break
        _s += c
    #print(repr(_s))
    return _s

g_demoSignature = MAKE_ID('ZCLD')

if NETWORK_ReadLong() != g_demoSignature:
    # old demo
    File.seek(-4, 1)
    demostart = NETWORK_ReadByte()
    print('CLD_DEMOSTART is %02X.' % (demostart))
    if NETWORK_ReadLong() == 12345678:
        File.seek(13, 0)
        ver = NETWORK_ReadString()
        print('Demo version \'%s\' is unsupported.' % (ver))
        exit(1)
    else:
        print('File doesn\'t look like a Zandronum demo.')
        exit(2)

if NETWORK_ReadUByte() != CLD_DEMOLENGTH:
    print('Expected CLD_DEMOLENGTH.')
    exit(2)
    
g_lDemoLength = NETWORK_ReadLong()
(t_w, t_h) = console.getTerminalSize()
print('='*(t_w-1))
print(' Demo length is %d bytes.' % (g_lDemoLength))
print('-'*(t_w-1))
      
while True:
    lCommand = NETWORK_ReadUByte()
    
    #print('lCommand = %02X (%u)' % (lCommand, lCommand))
    
    if lCommand == CLD_DEMOVERSION:
        lDemoVersion = NETWORK_ReadUShort()
        print('Version %s demo.' % (NETWORK_ReadString()))
        NETWORK_ReadByte()
        NETWORK_ReadLong()
    elif lCommand == CLD_DEMOWADS:
        count = NETWORK_ReadUShort()
        wad_names = []
        for i in range(count):
            print('WAD used: %s' % (NETWORK_ReadString()))
        print('Demo checksum: %s' % (NETWORK_ReadString()))
        print('Map checksum: %s' % (NETWORK_ReadString()))
    elif lCommand == CLD_USERINFO:
        u_name = NETWORK_ReadString()
        u_gender = NETWORK_ReadUByte()
        u_color = NETWORK_ReadULong()
        u_aimdist = NETWORK_ReadULong()
        u_skin = NETWORK_ReadString()
        u_railColor = NETWORK_ReadULong()
        u_handicap = NETWORK_ReadUByte()
        u_unlagged = NETWORK_ReadUByte() != 0
        u_respawnOnFire = NETWORK_ReadUByte() != 0
        u_ticsPerUpdate = NETWORK_ReadUByte()
        u_connectionType = NETWORK_ReadUByte()
        u_playerClass = NETWORK_ReadString()
        
        # own userinfo!
        data = {}
        data['u_player'] = 0
        data['u_name'] = u_name
        data['u_gender'] = u_gender
        data['u_color'] = u_color
        data['u_aimdist'] = u_aimdist
        data['u_skin'] = u_skin
        data['u_railColor'] = u_railColor
        data['u_handicap'] = u_handicap
        data['u_unlagged'] = u_unlagged
        data['u_respawnOnFire'] = u_respawnOnFire
        data['u_ticsPerUpdate'] = u_ticsPerUpdate
        data['u_connectionType'] = u_connectionType
        data['u_playerClass'] = u_playerClass
        #cldproc.proc(SVC_SETPLAYERUSERINFO, 0, 'SVC_SETPLAYERUSERINFO', '', data) # fake the packet
        cldproc.proc(SVC_SETPLAYERUSERINFO, 0, data) # fake the packet
        
    elif lCommand == CLD_BODYSTART:
        break
    else:
        print('Erroneous command (%02X, %u).' % (lCommand, lCommand))
        break

print('='*(t_w-1))
print('')

while True:
    lCommand = 0
    lExtCommand = 0
    data = {}

    lCommand = NETWORK_ReadUByte()
    
    #print('lCommand = %02X (%u) @ %08X' % (lCommand, lCommand, File.tell()))
    #if lCommand == SVCC_AUTHENTICATE:
    #    auth += 1;
    #    if auth > 2:
    #        break
    
    if lCommand == SVC_NOTHING:
        pass
    elif lCommand == CLD_DEMOEND:
        print('')
        print('Demo ended.')
        break
    elif lCommand == CLD_TICCMD:
    #if lCommand == CLD_TICCMD:
        data['u_yaw'] = NETWORK_ReadShort()
        data['u_roll'] = NETWORK_ReadShort()
        data['u_pitch'] = NETWORK_ReadShort()
        data['u_buttons'] = NETWORK_ReadUByte()
        data['u_upmove'] = NETWORK_ReadShort()
        data['u_forwardmove'] = NETWORK_ReadShort()
        data['u_sidemove'] = NETWORK_ReadShort()
    elif lCommand == CLD_INVUSE:
        data['u_item'] = NETWORK_ReadString()
    elif lCommand == SVCC_AUTHENTICATE:
        #mapname
        data['u_map'] = NETWORK_ReadString()
    elif lCommand == SVCC_MAPLOAD:
        data['u_gameMode'] = NETWORK_ReadUByte()
    elif lCommand == SVC_PING:
        data['u_serverTime'] = NETWORK_ReadLong()
    elif lCommand == SVC_BEGINSNAPSHOT:
        pass
    elif lCommand == SVC_ENDSNAPSHOT:
        pass
    elif lCommand == SVC_SPAWNPLAYER or lCommand == SVC_SPAWNMORPHPLAYER:
        data['u_player'] = NETWORK_ReadByte()
        data['u_playerState'] = NETWORK_ReadByte()
        data['u_bot'] = NETWORK_ReadUByte() != 0
        data['u_state'] = NETWORK_ReadByte()
        data['u_spectating'] = NETWORK_ReadUByte() != 0
        data['u_deadSpectator'] = NETWORK_ReadUByte() != 0
        data['u_id'] = NETWORK_ReadShort()
        data['u_angle'] = NETWORK_ReadLong()
        data['u_x'] = NETWORK_ReadLong()
        data['u_y'] = NETWORK_ReadLong()
        data['u_z'] = NETWORK_ReadLong()
        data['u_playerClass'] = NETWORK_ReadShort()
        if lCommand == SVC_SPAWNMORPHPLAYER:
            data['u_actorNetworkIndex'] = NETWORK_ReadShort()
    elif lCommand == SVC_MOVEPLAYER:
        data['u_player'] = NETWORK_ReadByte()
        data['u_flags'] = NETWORK_ReadUByte()
        data['u_visible'] = (data['u_flags'] & PLAYER_VISIBLE)
        if data['u_visible']:
            data['u_x'] = NETWORK_ReadLong()
            data['u_y'] = NETWORK_ReadLong()
            data['u_z'] = NETWORK_ReadShort() << 16
            data['u_angle'] = NETWORK_ReadLong()
            data['u_momx'] = NETWORK_ReadShort() << 16
            data['u_momy'] = NETWORK_ReadShort() << 16
            data['u_momz'] = NETWORK_ReadShort() << 16
            data['u_crouching'] = NETWORK_ReadUByte() != 0
    elif lCommand == SVC_DAMAGEPLAYER:
        data['u_player'] = NETWORK_ReadByte()
        data['u_health'] = NETWORK_ReadShort()
        data['u_armor'] = NETWORK_ReadShort()
        data['u_inflictor'] = NETWORK_ReadShort()
    elif lCommand == SVC_KILLPLAYER:
        data['u_player'] = NETWORK_ReadByte()
        data['u_source'] = NETWORK_ReadShort()
        data['u_inflictor'] = NETWORK_ReadShort()
        data['u_health'] = NETWORK_ReadShort()
        data['u_mod'] = NETWORK_ReadString()
        data['u_damageType'] = NETWORK_ReadString()
        data['u_actorNetworkIndex'] = NETWORK_ReadShort()
    elif lCommand == SVC_SETPLAYERHEALTH:
        data['u_player'] = NETWORK_ReadByte()
        data['u_health'] = NETWORK_ReadShort()
    elif lCommand == SVC_SETPLAYERARMOR:
        data['u_player'] = NETWORK_ReadByte()
        data['u_armor'] = NETWORK_ReadShort()
        data['u_armorType'] = NETWORK_ReadString()
    elif lCommand == SVC_SETPLAYERSTATE:
        data['u_player'] = NETWORK_ReadByte()
        data['u_state'] = NETWORK_ReadUByte()
    elif lCommand == SVC_SETPLAYERUSERINFO:
        data['u_player'] = NETWORK_ReadByte()
        data['u_flags'] = NETWORK_ReadShort()
        if (data['u_flags'] & USERINFO_NAME) != 0:
            data['u_name'] = NETWORK_ReadString()
        if (data['u_flags'] & USERINFO_GENDER) != 0:
            data['u_gender'] = NETWORK_ReadByte()
        if (data['u_flags'] & USERINFO_COLOR) != 0:
            data['u_color'] = NETWORK_ReadLong()
        if (data['u_flags'] & USERINFO_RAILCOLOR) != 0:
            data['u_railColor'] = NETWORK_ReadLong()
        if (data['u_flags'] & USERINFO_SKIN) != 0:
            data['u_skin'] = NETWORK_ReadString()
        if (data['u_flags'] & USERINFO_HANDICAP) != 0:
            data['u_handicap'] = NETWORK_ReadByte()
        if (data['u_flags'] & USERINFO_UNLAGGED) != 0:
            data['u_unlagged'] = NETWORK_ReadUByte() != 0
        if (data['u_flags'] & USERINFO_RESPAWNONFIRE) != 0:
            data['u_respawnOnFire'] = NETWORK_ReadUByte() != 0
        if (data['u_flags'] & USERINFO_TICSPERUPDATE) != 0:
            data['u_ticsPerUpdate'] = NETWORK_ReadByte()
        if (data['u_flags'] & USERINFO_CONNECTIONTYPE) != 0:
            data['u_connectionType'] = NETWORK_ReadByte()
    elif lCommand == SVC_SETPLAYERFRAGS:
        data['u_player'] = NETWORK_ReadByte()
        data['u_frags'] = NETWORK_ReadShort()
    elif lCommand == SVC_SETPLAYERPOINTS:
        data['u_player'] = NETWORK_ReadByte()
        data['u_points'] = NETWORK_ReadShort()
    elif lCommand == SVC_SETPLAYERWINS:
        data['u_player'] = NETWORK_ReadByte()
        data['u_wins'] = NETWORK_ReadByte()
    elif lCommand == SVC_SETPLAYERKILLCOUNT:
        data['u_player'] = NETWORK_ReadByte()
        data['u_killCount'] = NETWORK_ReadShort()
    elif lCommand == SVC_SETPLAYERCHATSTATUS:
        data['u_player'] = NETWORK_ReadByte()
        data['u_chatting'] = NETWORK_ReadUByte() != 0
    elif lCommand == SVC_SETPLAYERCONSOLESTATUS:
        data['u_player'] = NETWORK_ReadByte()
        data['u_console'] = NETWORK_ReadUByte() != 0
    elif lCommand == SVC_SETPLAYERLAGGINGSTATUS:
        data['u_player'] = NETWORK_ReadByte()
        data['u_lagging'] = NETWORK_ReadUByte() != 0
    elif lCommand == SVC_SETPLAYERREADYTOGOONSTATUS:
        data['u_player'] = NETWORK_ReadByte()
        data['u_ready'] = NETWORK_ReadUByte() != 0
    elif lCommand == SVC_SETPLAYERTEAM:
        data['u_player'] = NETWORK_ReadByte()
        data['u_team'] = NETWORK_ReadByte()
    elif lCommand == SVC_SETPLAYERCAMERA:
        data['u_id'] = NETWORK_ReadShort()
        data['u_reversible'] = NETWORK_ReadUByte() != 0
    elif lCommand == SVC_SETPLAYERPOISONCOUNT:
        data['u_player'] = NETWORK_ReadByte()
        data['u_poisonCount'] = NETWORK_ReadShort()
    elif lCommand == SVC_SETPLAYERAMMOCAPACITY:
        data['u_player'] = NETWORK_ReadByte()
        data['u_actorNetworkIndex'] = NETWORK_ReadShort()
        data['u_maxAmount'] = NETWORK_ReadShort()
    elif lCommand == SVC_SETPLAYERCHEATS:
        data['u_player'] = NETWORK_ReadByte()
        data['u_cheats'] = NETWORK_ReadLong()
    elif lCommand == SVC_SETPLAYERPENDINGWEAPON:
        data['u_player'] = NETWORK_ReadByte()
        data['u_actorNetworkIndex'] = NETWORK_ReadShort()
    elif lCommand == SVC_SETPLAYERPSPRITE:
        data['u_player'] = NETWORK_ReadByte()
        data['u_state'] = NETWORK_ReadString()
        data['u_offset'] = NETWORK_ReadByte()
        data['u_position'] = NETWORK_ReadByte()
    elif lCommand == SVC_SETPLAYERBLEND:
        data['u_player'] = NETWORK_ReadByte()
        data['u_blendR'] = NETWORK_ReadFloat()
        data['u_blendG'] = NETWORK_ReadFloat()
        data['u_blendB'] = NETWORK_ReadFloat()
        data['u_blendA'] = NETWORK_ReadFloat()
    elif lCommand == SVC_SETPLAYERMAXHEALTH:
        data['u_player'] = NETWORK_ReadByte()
        data['u_maxHealth'] = NETWORK_ReadLong()
    elif lCommand == SVC_SETPLAYERLIVESLEFT:
        data['u_player'] = NETWORK_ReadByte()
        data['u_lives'] = NETWORK_ReadByte()
    elif lCommand == SVC_UPDATEPLAYERPING:
        data['u_player'] = NETWORK_ReadByte()
        data['u_ping'] = NETWORK_ReadShort()
    elif lCommand == SVC_UPDATEPLAYEREXTRADATA:
        data['u_player'] = NETWORK_ReadByte()
        data['u_pitch'] = NETWORK_ReadLong()
        data['u_waterLevel'] = NETWORK_ReadByte()
        data['u_buttons'] = NETWORK_ReadByte()
        data['u_viewZ'] = NETWORK_ReadLong()
        data['u_bob'] = NETWORK_ReadLong()
    elif lCommand == SVC_UPDATEPLAYERTIME:
        data['u_player'] = NETWORK_ReadByte()
        data['u_time'] = NETWORK_ReadShort()
    elif lCommand == SVC_MOVELOCALPLAYER:
        data['u_ticOnServerEnd'] = NETWORK_ReadLong()
        data['u_x'] = NETWORK_ReadLong()
        data['u_y'] = NETWORK_ReadLong()
        data['u_z'] = NETWORK_ReadLong()
        data['u_momx'] = NETWORK_ReadLong()
        data['u_momy'] = NETWORK_ReadLong()
        data['u_momz'] = NETWORK_ReadLong()
    elif lCommand == SVC_DISCONNECTPLAYER:
        data['u_player'] = NETWORK_ReadByte()
    elif lCommand == SVC_SETCONSOLEPLAYER:
        data['u_player'] = NETWORK_ReadByte()
    elif lCommand == SVC_CONSOLEPLAYERKICKED:
        pass
    elif lCommand == SVC_GIVEPLAYERMEDAL:
        data['u_player'] = NETWORK_ReadByte()
        data['u_medal'] = NETWORK_ReadByte()
    elif lCommand == SVC_RESETALLPLAYERSFRAGCOUNT:
        pass
    elif lCommand == SVC_PLAYERISSPECTATOR:
        data['u_player'] = NETWORK_ReadByte()
        data['u_deadSpectator'] = NETWORK_ReadUByte() != 0
    elif lCommand == SVC_PLAYERSAY:
        data['u_player'] = NETWORK_ReadByte()
        data['u_mode'] = NETWORK_ReadByte()
        data['u_string'] = NETWORK_ReadString()
    elif lCommand == SVC_PLAYERTAUNT:
        data['u_player'] = NETWORK_ReadByte()
    elif lCommand == SVC_PLAYERRESPAWNINVULNERABILITY:
        data['u_player'] = NETWORK_ReadByte()
    elif lCommand == SVC_PLAYERUSEINVENTORY:
        data['u_player'] = NETWORK_ReadByte()
        data['u_actorNetworkIndex'] = NETWORK_ReadShort()
    elif lCommand == SVC_PLAYERDROPINVENTORY:
        data['u_player'] = NETWORK_ReadByte()
        data['u_actorNetworkIndex'] = NETWORK_ReadShort()
    elif lCommand == SVC_SPAWNTHING:
        data['u_x'] = NETWORK_ReadShort() << 16
        data['u_y'] = NETWORK_ReadShort() << 16
        data['u_z'] = NETWORK_ReadShort() << 16
        data['u_actorNetworkIndex'] = NETWORK_ReadShort()
        data['u_id'] = NETWORK_ReadShort()
    elif lCommand == SVC_SPAWNTHINGNONETID:
        data['u_x'] = NETWORK_ReadShort() << 16
        data['u_y'] = NETWORK_ReadShort() << 16
        data['u_z'] = NETWORK_ReadShort() << 16
        data['u_actorNetworkIndex'] = NETWORK_ReadShort()
    elif lCommand == SVC_SPAWNTHINGEXACT:
        data['u_x'] = NETWORK_ReadShort()
        data['u_y'] = NETWORK_ReadShort()
        data['u_z'] = NETWORK_ReadShort()
        data['u_actorNetworkIndex'] = NETWORK_ReadShort()
        data['u_id'] = NETWORK_ReadShort()
    elif lCommand == SVC_SPAWNTHINGEXACTNONETID:
        data['u_x'] = NETWORK_ReadShort()
        data['u_y'] = NETWORK_ReadShort()
        data['u_z'] = NETWORK_ReadShort()
        data['u_actorNetworkIndex'] = NETWORK_ReadShort()
    elif lCommand == SVC_MOVETHING:
        data['u_id'] = NETWORK_ReadShort()
        data['u_bits'] = NETWORK_ReadUShort()
        if (data['u_bits'] & CM_X) != 0:
            data['u_x'] = NETWORK_ReadShort() << 16
        if (data['u_bits'] & CM_Y) != 0:
            data['u_y'] = NETWORK_ReadShort() << 16
        if (data['u_bits'] & CM_Z) != 0:
            data['u_z'] = NETWORK_ReadShort() << 16
        if (data['u_bits'] & CM_LAST_X) != 0:
            data['u_lastX'] = NETWORK_ReadShort() << 16
        if (data['u_bits'] & CM_LAST_Y) != 0:
            data['u_lastY'] = NETWORK_ReadShort() << 16
        if (data['u_bits'] & CM_LAST_Z) != 0:
            data['u_lastZ'] = NETWORK_ReadShort() << 16
        if (data['u_bits'] & CM_ANGLE) != 0:
            data['u_angle'] = NETWORK_ReadLong()
        if (data['u_bits'] & CM_MOMX) != 0:
            data['u_momX'] = NETWORK_ReadShort() << 16
        if (data['u_bits'] & CM_MOMY) != 0:
            data['u_momY'] = NETWORK_ReadShort() << 16
        if (data['u_bits'] & CM_MOMZ) != 0:
            data['u_momZ'] = NETWORK_ReadShort() << 16
        if (data['u_bits'] & CM_PITCH) != 0:
            data['u_pitch'] = NETWORK_ReadLong()
        if (data['u_bits'] & CM_MOVEDIR) != 0:
            data['u_movedir'] = NETWORK_ReadByte()
    elif lCommand == SVC_MOVETHINGEXACT:
        data['u_id'] = NETWORK_ReadShort()
        data['u_bits'] = NETWORK_ReadUShort()
        if (data['u_bits'] & CM_X) != 0:
            data['u_x'] = NETWORK_ReadLong()
        if (data['u_bits'] & CM_Y) != 0:
            data['u_y'] = NETWORK_ReadLong()
        if (data['u_bits'] & CM_Z) != 0:
            data['u_z'] = NETWORK_ReadLong()
        if (data['u_bits'] & CM_LAST_X) != 0:
            data['u_lastX'] = NETWORK_ReadLong()
        if (data['u_bits'] & CM_LAST_Y) != 0:
            data['u_lastY'] = NETWORK_ReadLong()
        if (data['u_bits'] & CM_LAST_Z) != 0:
            data['u_lastZ'] = NETWORK_ReadLong()
        if (data['u_bits'] & CM_ANGLE) != 0:
            data['u_angle'] = NETWORK_ReadLong()
        if (data['u_bits'] & CM_MOMX) != 0:
            data['u_momX'] = NETWORK_ReadLong() 
        if (data['u_bits'] & CM_MOMY) != 0:
            data['u_momY'] = NETWORK_ReadLong()
        if (data['u_bits'] & CM_MOMZ) != 0:
            data['u_momZ'] = NETWORK_ReadLong()
        if (data['u_bits'] & CM_PITCH) != 0:
            data['u_pitch'] = NETWORK_ReadLong()
        if (data['u_bits'] & CM_MOVEDIR) != 0:
            data['u_movedir'] = NETWORK_ReadByte()
    elif lCommand == SVC_DAMAGETHING:
        data['u_id'] = NETWORK_ReadShort()
    elif lCommand == SVC_KILLTHING:
        data['u_id'] = NETWORK_ReadShort()
        data['u_health'] = NETWORK_ReadShort()
        data['u_damageType'] = NETWORK_ReadString()
        data['u_source'] = NETWORK_ReadShort()
        data['u_inflictor'] = NETWORK_ReadShort()
    elif lCommand == SVC_SETTHINGSTATE:
        data['u_id'] = NETWORK_ReadShort()
        data['u_state'] = NETWORK_ReadByte()
    elif lCommand == SVC_SETTHINGTARGET:
        data['u_id'] = NETWORK_ReadShort()
        data['u_target'] = NETWORK_ReadShort()
    elif lCommand == SVC_DESTROYTHING:
        data['u_id'] = NETWORK_ReadShort()
    elif lCommand == SVC_SETTHINGANGLE:
        data['u_id'] = NETWORK_ReadShort()
        data['u_angle'] = NETWORK_ReadShort() << 16
    elif lCommand == SVC_SETTHINGANGLEEXACT:
        data['u_id'] = NETWORK_ReadShort()
        data['u_angle'] = NETWORK_ReadLong()
    elif lCommand == SVC_SETTHINGWATERLEVEL:
        data['u_id'] = NETWORK_ReadShort()
        data['u_waterLevel'] = NETWORK_ReadByte()
    elif lCommand == SVC_SETTHINGFLAGS:
        data['u_id'] = NETWORK_ReadShort()
        data['u_flagSet'] = NETWORK_ReadByte()
        data['u_flags'] = NETWORK_ReadULong()
    elif lCommand == SVC_SETTHINGARGUMENTS:
        data['u_id'] = NETWORK_ReadShort()
        data['u_arg0'] = NETWORK_ReadByte()
        data['u_arg1'] = NETWORK_ReadByte()
        data['u_arg2'] = NETWORK_ReadByte()
        data['u_arg3'] = NETWORK_ReadByte()
        data['u_arg4'] = NETWORK_ReadByte()
    elif lCommand == SVC_SETTHINGTRANSLATION:
        data['u_id'] = NETWORK_ReadShort()
        data['u_translation'] = NETWORK_ReadLong()
    elif lCommand == SVC_SETTHINGPROPERTY:
        data['u_id'] = NETWORK_ReadShort()
        data['u_property'] = NETWORK_ReadByte()
        data['u_value'] = NETWORK_ReadLong()
    elif lCommand == SVC_SETTHINGSOUND:
        data['u_id'] = NETWORK_ReadShort()
        data['u_soundWhich'] = NETWORK_ReadByte()
        data['u_sound'] = NETWORK_ReadString()
    elif lCommand == SVC_SETTHINGSPAWNPOINT:
        data['u_id'] = NETWORK_ReadShort()
        data['u_x'] = NETWORK_ReadLong()
        data['u_y'] = NETWORK_ReadLong()
        data['u_z'] = NETWORK_ReadLong()
    elif lCommand == SVC_SETTHINGSPECIAL1:
        data['u_id'] = NETWORK_ReadShort()
        data['u_special'] = NETWORK_ReadShort()
    elif lCommand == SVC_SETTHINGSPECIAL2:
        data['u_id'] = NETWORK_ReadShort()
        data['u_special'] = NETWORK_ReadShort()
    elif lCommand == SVC_SETTHINGTICS:
        data['u_id'] = NETWORK_ReadShort()
        data['u_tics'] = NETWORK_ReadShort()
    elif lCommand == SVC_SETTHINGTID:
        data['u_id'] = NETWORK_ReadShort()
        data['u_tid'] = NETWORK_ReadShort()
    elif lCommand == SVC_SETTHINGGRAVITY:
        data['u_id'] = NETWORK_ReadShort()
        data['u_gravity'] = NETWORK_ReadLong()
    elif lCommand == SVC_SETTHINGFRAME or lCommand == SVC_SETTHINGFRAMENF:
        data['u_id'] = NETWORK_ReadShort()
        data['u_state'] = NETWORK_ReadString()
        data['u_offset'] = NETWORK_ReadByte()
    elif lCommand == SVC_SETWEAPONAMMOGIVE:
        data['u_id'] = NETWORK_ReadShort()
        data['u_ammoGive1'] = NETWORK_ReadShort()
        data['u_ammoGive2'] = NETWORK_ReadShort()
    elif lCommand == SVC_THINGISCORPSE:
        data['u_id'] = NETWORK_ReadShort()
        data['u_monster'] = NETWORK_ReadUByte() != 0
    elif lCommand == SVC_HIDETHING:
        data['u_id'] = NETWORK_ReadShort()
    elif lCommand == SVC_TELEPORTTHING:
        data['u_id'] = NETWORK_ReadShort()
        data['u_newX'] = NETWORK_ReadShort() << 16
        data['u_newY'] = NETWORK_ReadShort() << 16
        data['u_newZ'] = NETWORK_ReadShort() << 16
        data['u_newMomX'] = NETWORK_ReadShort() << 16
        data['u_newMomY'] = NETWORK_ReadShort() << 16
        data['u_newMomZ'] = NETWORK_ReadShort() << 16
        data['u_newReactionTime'] = NETWORK_ReadShort()
        data['u_newAngle'] = NETWORK_ReadLong()
        data['u_sourceFog'] = NETWORK_ReadUByte() != 0
        data['u_destFog'] = NETWORK_ReadUByte() != 0
        data['u_teleZoom'] = NETWORK_ReadUByte() != 0
    elif lCommand == SVC_THINGACTIVATE:
        data['u_id'] = NETWORK_ReadShort()
        data['u_activator'] = NETWORK_ReadShort()
    elif lCommand == SVC_THINGDEACTIVATE:
        data['u_id'] = NETWORK_ReadShort()
        data['u_deactivator'] = NETWORK_ReadShort()
    elif lCommand == SVC_RESPAWNDOOMTHING:
        data['u_id'] = NETWORK_ReadShort()
        data['u_fog'] = NETWORK_ReadUByte() != 0
    elif lCommand == SVC_RESPAWNRAVENTHING:
        data['u_id'] = NETWORK_ReadShort()
    elif lCommand == SVC_SPAWNBLOOD:
        data['u_x'] = NETWORK_ReadShort() << 16
        data['u_y'] = NETWORK_ReadShort() << 16
        data['u_z'] = NETWORK_ReadShort() << 16
        data['u_dir'] = NETWORK_ReadShort() << 16
        data['u_damage'] = NETWORK_ReadByte()
        data['u_id'] = NETWORK_ReadShort()
    elif lCommand == SVC_SPAWNBLOODSPLATTER or lCommand == SVC_SPAWNBLOODSPLATTER2:
        data['u_x'] = NETWORK_ReadShort() << 16
        data['u_y'] = NETWORK_ReadShort() << 16
        data['u_z'] = NETWORK_ReadShort() << 16
        data['u_id'] = NETWORK_ReadShort()
    elif lCommand == SVC_SPAWNPUFF:
        data['u_x'] = NETWORK_ReadShort() << 16
        data['u_y'] = NETWORK_ReadShort() << 16
        data['u_z'] = NETWORK_ReadShort() << 16
        data['u_actorNetworkIndex'] = NETWORK_ReadShort()
        data['u_state'] = NETWORK_ReadByte()
        data['u_receiveTranslation'] = NETWORK_ReadUByte() != 0
    elif lCommand == SVC_PRINT:
        data['u_printLevel'] = NETWORK_ReadByte()
        data['u_string'] = NETWORK_ReadString()
    elif lCommand == SVC_PRINTMID:
        data['u_string'] = NETWORK_ReadString()
        data['u_bold'] = NETWORK_ReadUByte() != 0
    elif lCommand == SVC_PRINTMOTD:
        data['u_string'] = NETWORK_ReadString()
    elif lCommand == SVC_PRINTHUDMESSAGE:
        data['u_text'] = NETWORK_ReadString()
        data['u_fX'] = NETWORK_ReadFloat()
        data['u_fY'] = NETWORK_ReadFloat()
        data['u_hudWidth'] = NETWORK_ReadShort()
        data['u_hudHeight'] = NETWORK_ReadShort()
        data['u_color'] = NETWORK_ReadByte()
        data['u_holdTime'] = NETWORK_ReadFloat()
        data['u_font'] = NETWORK_ReadString()
        data['u_log'] = NETWORK_ReadUByte() != 0
        data['u_id'] = NETWORK_ReadLong()
    elif lCommand == SVC_PRINTHUDMESSAGEFADEOUT:
        data['u_text'] = NETWORK_ReadString()
        data['u_fX'] = NETWORK_ReadFloat()
        data['u_fY'] = NETWORK_ReadFloat()
        data['u_hudWidth'] = NETWORK_ReadShort()
        data['u_hudHeight'] = NETWORK_ReadShort()
        data['u_color'] = NETWORK_ReadByte()
        data['u_holdTime'] = NETWORK_ReadFloat()
        data['u_fadeOutTime'] = NETWORK_ReadFloat()
        data['u_font'] = NETWORK_ReadString()
        data['u_log'] = NETWORK_ReadUByte() != 0
        data['u_id'] = NETWORK_ReadLong()
    elif lCommand == SVC_PRINTHUDMESSAGEFADEINOUT:
        data['u_text'] = NETWORK_ReadString()
        data['u_fX'] = NETWORK_ReadFloat()
        data['u_fY'] = NETWORK_ReadFloat()
        data['u_hudWidth'] = NETWORK_ReadShort()
        data['u_hudHeight'] = NETWORK_ReadShort()
        data['u_color'] = NETWORK_ReadByte()
        data['u_holdTime'] = NETWORK_ReadFloat()
        data['u_fadeInTime'] = NETWORK_ReadFloat()
        data['u_fadeOutTime'] = NETWORK_ReadFloat()
        data['u_font'] = NETWORK_ReadString()
        data['u_log'] = NETWORK_ReadUByte() != 0
        data['u_id'] = NETWORK_ReadLong()
    elif lCommand == SVC_PRINTHUDMESSAGETYPEONFADEOUT:
        data['u_text'] = NETWORK_ReadString()
        data['u_fX'] = NETWORK_ReadFloat()
        data['u_fY'] = NETWORK_ReadFloat()
        data['u_hudWidth'] = NETWORK_ReadShort()
        data['u_hudHeight'] = NETWORK_ReadShort()
        data['u_color'] = NETWORK_ReadByte()
        data['u_typeOnTime'] = NETWORK_ReadFloat()
        data['u_holdTime'] = NETWORK_ReadFloat()
        data['u_fadeOutTime'] = NETWORK_ReadFloat()
        data['u_font'] = NETWORK_ReadString()
        data['u_log'] = NETWORK_ReadUByte() != 0
        data['u_id'] = NETWORK_ReadLong()
    elif lCommand == SVC_SETGAMEMODE:
        data['u_gameMode'] = NETWORK_ReadByte()
        data['u_instagib'] = NETWORK_ReadUByte() != 0
        data['u_buckshot'] = NETWORK_ReadUByte() != 0
    elif lCommand == SVC_SETGAMESKILL:
        data['u_skill'] = NETWORK_ReadByte()
        data['u_botskill'] = NETWORK_ReadByte()
    elif lCommand == SVC_SETGAMEDMFLAGS:
        data['u_dmflags'] = NETWORK_ReadULong()
        data['u_dmflags2'] = NETWORK_ReadULong()
        data['u_compatflags'] = NETWORK_ReadULong()
        data['u_compatflags2'] = NETWORK_ReadULong()
        data['u_dmflags3'] = NETWORK_ReadULong()
    elif lCommand == SVC_SETGAMEMODELIMITS:
        data['u_fraglimit'] = NETWORK_ReadShort()
        data['u_timelimit'] = NETWORK_ReadFloat()
        data['u_pointlimit'] = NETWORK_ReadShort()
        data['u_duellimit'] = NETWORK_ReadByte()
        data['u_winlimit'] = NETWORK_ReadByte()
        data['u_wavelimit'] = NETWORK_ReadByte()
        data['u_sv_cheats'] = NETWORK_ReadByte()
        data['u_sv_fastweapons'] = NETWORK_ReadByte()
        data['u_sv_maxlives'] = NETWORK_ReadByte()
        data['u_sv_maxteams'] = NETWORK_ReadByte()
        data['u_sv_gravity'] = NETWORK_ReadFloat()
        data['u_sv_aircontrol'] = NETWORK_ReadFloat()
        data['u_sv_coop_damagefactor'] = NETWORK_ReadFloat()
        data['u_alwaysapplydmflags'] = NETWORK_ReadUByte() != 0
    elif lCommand == SVC_SETGAMEENDLEVELDELAY:
        data['u_delay'] = NETWORK_ReadShort()
    elif lCommand == SVC_SETGAMEMODESTATE:
        data['u_state'] = NETWORK_ReadByte()
        data['u_countdown'] = NETWORK_ReadShort()
    elif lCommand == SVC_SETDUELNUMDUELS:
        data['u_numDuels'] = NETWORK_ReadByte()
    elif lCommand == SVC_SETLMSSPECTATORSETTINGS:
        data['u_lmsspectatorsettings'] = NETWORK_ReadULong()
    elif lCommand == SVC_SETLMSALLOWEDWEAPONS:
        data['u_lmsallowedweapons'] = NETWORK_ReadULong()
    elif lCommand == SVC_SETINVASIONNUMMONSTERSLEFT:
        data['u_monstersLeft'] = NETWORK_ReadShort()
        data['u_vilesLeft'] = NETWORK_ReadShort()
    elif lCommand == SVC_SETINVASIONWAVE:
        data['u_wave'] = NETWORK_ReadByte()
    elif lCommand == SVC_SETSIMPLECTFSTMODE:
        data['u_simpleCTFST'] = NETWORK_ReadUByte() != 0
    elif lCommand == SVC_DOPOSSESSIONARTIFACTPICKEDUP:
        data['u_player'] = NETWORK_ReadByte()
        data['u_ticks'] = NETWORK_ReadShort()
    elif lCommand == SVC_DOPOSSESSIONARTIFACTDROPPED:
        pass
    elif lCommand == SVC_DOGAMEMODEFIGHT:
        data['u_wave'] = NETWORK_ReadByte()
    elif lCommand == SVC_DOGAMEMODECOUNTDOWN:
        data['u_ticks'] = NETWORK_ReadShort()
    elif lCommand == SVC_DOGAMEMODEWINSEQUENCE:
        data['u_winner'] = NETWORK_ReadByte()
    elif lCommand == SVC_SETDOMINATIONSTATE:
        data['u_points'] = NETWORK_ReadLong()
        data['u_pointOwners'] = []
        for i in range(0, data['u_points']):
            data['u_pointOwners'].append(NETWORK_ReadByte())
    elif lCommand == SVC_SETDOMINATIONPOINTOWNER:
        data['u_point'] = NETWORK_ReadByte()
        data['u_player'] = NETWORK_ReadByte()
    elif lCommand == SVC_SETTEAMFRAGS:
        data['u_team'] = NETWORK_ReadByte()
        data['u_frags'] = NETWORK_ReadShort()
        data['u_announce'] = NETWORK_ReadUByte() != 0
    elif lCommand == SVC_SETTEAMSCORE:
        data['u_team'] = NETWORK_ReadByte()
        data['u_score'] = NETWORK_ReadShort()
        data['u_announce'] = NETWORK_ReadUByte() != 0
    elif lCommand == SVC_SETTEAMWINS:
        data['u_team'] = NETWORK_ReadByte()
        data['u_wins'] = NETWORK_ReadShort()
        data['u_announce'] = NETWORK_ReadUByte() != 0
    elif lCommand == SVC_SETTEAMRETURNTICKS:
        data['u_team'] = NETWORK_ReadByte()
        data['u_ticks'] = NETWORK_ReadShort()
    elif lCommand == SVC_TEAMFLAGRETURNED:
        data['u_team'] = NETWORK_ReadByte()
    elif lCommand == SVC_TEAMFLAGDROPPED:
        data['u_player'] = NETWORK_ReadByte() # who dropped
        data['u_team'] = NETWORK_ReadByte()
    elif lCommand == SVC_SPAWNMISSILE:
        data['u_x'] = NETWORK_ReadShort() << 16
        data['u_y'] = NETWORK_ReadShort() << 16
        data['u_z'] = NETWORK_ReadShort() << 16
        data['u_momX'] = NETWORK_ReadLong()
        data['u_momY'] = NETWORK_ReadLong()
        data['u_momZ'] = NETWORK_ReadLong()
        data['u_actorNetworkIndex'] = NETWORK_ReadShort()
        data['u_id'] = NETWORK_ReadShort()
        data['u_target'] = NETWORK_ReadShort()
    elif lCommand == SVC_SPAWNMISSILEEXACT:
        data['u_x'] = NETWORK_ReadLong()
        data['u_y'] = NETWORK_ReadLong()
        data['u_z'] = NETWORK_ReadLong()
        data['u_momX'] = NETWORK_ReadLong()
        data['u_momY'] = NETWORK_ReadLong()
        data['u_momZ'] = NETWORK_ReadLong()
        data['u_actorNetworkIndex'] = NETWORK_ReadShort()
        data['u_id'] = NETWORK_ReadShort()
        data['u_target'] = NETWORK_ReadShort()
    elif lCommand == SVC_MISSILEEXPLODE:
        data['u_id'] = NETWORK_ReadShort()
        data['u_line'] = NETWORK_ReadShort()
        data['u_x'] = NETWORK_ReadShort() << 16
        data['u_y'] = NETWORK_ReadShort() << 16
        data['u_z'] = NETWORK_ReadShort() << 16
    elif lCommand == SVC_WEAPONSOUND:
        data['u_player'] = NETWORK_ReadByte()
        data['u_sound'] = NETWORK_ReadString()
    elif lCommand == SVC_WEAPONCHANGE:
        data['u_player'] = NETWORK_ReadByte()
        data['u_actorNetworkIndex'] = NETWORK_ReadShort()
    elif lCommand == SVC_WEAPONRAILGUN:
        data['u_id'] = NETWORK_ReadShort() # source actor
        data['u_startX'] = NETWORK_ReadFloat()
        data['u_startY'] = NETWORK_ReadFloat()
        data['u_startZ'] = NETWORK_ReadFloat()
        data['u_endX'] = NETWORK_ReadFloat()
        data['u_endY'] = NETWORK_ReadFloat()
        data['u_endZ'] = NETWORK_ReadFloat()
        data['u_color1'] = NETWORK_ReadULong()
        data['u_color2'] = NETWORK_ReadULong()
        data['u_maxDiff'] = NETWORK_ReadFloat()
        data['u_silent'] = NETWORK_ReadUByte() != 0
    elif lCommand == SVC_SETSECTORFLOORPLANE:
        data['u_sector'] = NETWORK_ReadShort()
        data['u_height'] = NETWORK_ReadShort()
    elif lCommand == SVC_SETSECTORCEILINGPLANE:
        data['u_sector'] = NETWORK_ReadShort()
        data['u_height'] = NETWORK_ReadShort()
    elif lCommand == SVC_SETSECTORFLOORPLANESLOPE:
        data['u_sector'] = NETWORK_ReadShort()
        data['u_lA'] = NETWORK_ReadShort() << 16
        data['u_lB'] = NETWORK_ReadShort() << 16
        data['u_lC'] = NETWORK_ReadShort() << 16
    elif lCommand == SVC_SETSECTORCEILINGPLANESLOPE:
        data['u_sector'] = NETWORK_ReadShort()
        data['u_lA'] = NETWORK_ReadShort() << 16
        data['u_lB'] = NETWORK_ReadShort() << 16
        data['u_lC'] = NETWORK_ReadShort() << 16
    elif lCommand == SVC_SETSECTORLIGHTLEVEL:
        data['u_sector'] = NETWORK_ReadShort()
        data['u_light'] = NETWORK_ReadUByte()
    elif lCommand == SVC_SETSECTORCOLOR or lCommand == SVC_SETSECTORCOLORBYTAG:
        data['u_sector'] = NETWORK_ReadShort()
        data['u_lR'] = NETWORK_ReadUByte()
        data['u_lG'] = NETWORK_ReadUByte()
        data['u_lB'] = NETWORK_ReadUByte()
        data['u_lDesaturate'] = NETWORK_ReadUByte()
    elif lCommand == SVC_SETSECTORFADE:
        data['u_sector'] = NETWORK_ReadShort()
        data['u_lR'] = NETWORK_ReadUByte()
        data['u_lG'] = NETWORK_ReadUByte()
        data['u_lB'] = NETWORK_ReadUByte()
    elif lCommand == SVC_SETSECTORFLAT:
        data['u_sector'] = NETWORK_ReadShort()
        data['u_ceiling'] = NETWORK_ReadString()
        data['u_floor'] = NETWORK_ReadString()
    elif lCommand == SVC_SETSECTORPANNING:
        data['u_sector'] = NETWORK_ReadShort()
        data['u_ceilingX'] = NETWORK_ReadShort()
        data['u_ceilingY'] = NETWORK_ReadShort()
        data['u_floorX'] = NETWORK_ReadShort()
        data['u_floorY'] = NETWORK_ReadShort()
    elif lCommand == SVC_SETSECTORROTATION:
        data['u_sector'] = NETWORK_ReadShort()
        data['u_ceilingAngle'] = NETWORK_ReadShort()
        data['u_floorAngle'] = NETWORK_ReadShort()
    elif lCommand == SVC_SETSECTORSCALE:
        data['u_sector'] = NETWORK_ReadShort()
        data['u_ceilingXScale'] = NETWORK_ReadShort() << 16
        data['u_ceilingYScale'] = NETWORK_ReadShort() << 16
        data['u_floorXScale'] = NETWORK_ReadShort() << 16
        data['u_floorYScale'] = NETWORK_ReadShort() << 16
    elif lCommand == SVC_SETSECTORSPECIAL:
        data['u_sector'] = NETWORK_ReadShort()
        data['u_special'] = NETWORK_ReadShort()
    elif lCommand == SVC_SETSECTORFRICTION:
        data['u_sector'] = NETWORK_ReadShort()
        data['u_friction'] = NETWORK_ReadLong()
        data['u_moveFactor'] = NETWORK_ReadLong()
    elif lCommand == SVC_SETSECTORANGLEYOFFSET:
        data['u_sector'] = NETWORK_ReadShort()
        data['u_ceilingAngle'] = NETWORK_ReadLong()
        data['u_ceilingYOffset'] = NETWORK_ReadLong()
        data['u_floorAngle'] = NETWORK_ReadLong()
        data['u_floorYOffset'] = NETWORK_ReadLong()
    elif lCommand == SVC_SETSECTORGRAVITY:
        data['u_sector'] = NETWORK_ReadShort()
        data['u_gravity'] = NETWORK_ReadFloat()
    elif lCommand == SVC_SETSECTORREFLECTION:
        data['u_sector'] = NETWORK_ReadShort()
        data['u_ceilingReflect'] = NETWORK_ReadFloat()
        data['u_floorReflect'] = NETWORK_ReadFloat()
    elif lCommand == SVC_STOPSECTORLIGHTEFFECT:
        data['u_sector'] = NETWORK_ReadShort()
    elif lCommand == SVC_DESTROYALLSECTORMOVERS:
        pass
    elif lCommand == SVC_SETSECTORLINK:
        data['u_sector'] = NETWORK_ReadShort()
        data['u_arg1'] = NETWORK_ReadShort()
        data['u_arg2'] = NETWORK_ReadByte()
        data['u_arg3'] = NETWORK_ReadByte()
    elif lCommand == SVC_DOSECTORLIGHTFIREFLICKER:
        data['u_sector'] = NETWORK_ReadShort()
        data['u_maxLight'] = NETWORK_ReadByte()
        data['u_minLight'] = NETWORK_ReadByte()
    elif lCommand == SVC_DOSECTORLIGHTFLICKER:
        data['u_sector'] = NETWORK_ReadShort()
        data['u_maxLight'] = NETWORK_ReadByte()
        data['u_minLight'] = NETWORK_ReadByte()
    elif lCommand == SVC_DOSECTORLIGHTLIGHTFLASH:
        data['u_sector'] = NETWORK_ReadShort()
        data['u_maxLight'] = NETWORK_ReadByte()
        data['u_minLight'] = NETWORK_ReadByte()
    elif lCommand == SVC_DOSECTORLIGHTSTROBE:
        data['u_sector'] = NETWORK_ReadShort()
        data['u_darkTime'] = NETWORK_ReadShort()
        data['u_brightTime'] = NETWORK_ReadShort()
        data['u_maxLight'] = NETWORK_ReadByte()
        data['u_minLight'] = NETWORK_ReadByte()
        data['u_count'] = NETWORK_ReadByte()
    elif lCommand == SVC_DOSECTORLIGHTGLOW:
        data['u_sector'] = NETWORK_ReadShort()
    elif lCommand == SVC_DOSECTORLIGHTGLOW2:
        data['u_sector'] = NETWORK_ReadShort()
        data['u_start'] = NETWORK_ReadByte()
        data['u_end'] = NETWORK_ReadByte()
        data['u_tics'] = NETWORK_ReadShort()
        data['u_maxTics'] = NETWORK_ReadShort()
        data['u_oneShot'] = NETWORK_ReadUByte() != 0
    elif lCommand == SVC_DOSECTORLIGHTPHASED:
        data['u_sector'] = NETWORK_ReadShort()
        data['u_baseLevel'] = NETWORK_ReadByte()
        data['u_phase'] = NETWORK_ReadByte()
    elif lCommand == SVC_SETLINEALPHA:
        data['u_line'] = NETWORK_ReadShort()
        data['u_alpha'] = NETWORK_ReadLong()
    elif lCommand == SVC_SETLINETEXTURE:
        data['u_line'] = NETWORK_ReadShort()
        data['u_texture'] = NETWORK_ReadString()
        data['u_side'] = NETWORK_ReadUByte() != 0
        data['u_position'] = NETWORK_ReadByte()
    elif lCommand == SVC_SETSOMELINEFLAGS:
        data['u_line'] = NETWORK_ReadShort()
        data['u_blockFlags'] = NETWORK_ReadULong()
    elif lCommand == SVC_SETSIDEFLAGS:
        data['u_side'] = NETWORK_ReadShort()
        data['u_flags'] = NETWORK_ReadUByte()
    elif lCommand == SVC_ACSSCRIPTEXECUTE:
        data['u_script'] = NETWORK_ReadShort()
        data['u_id'] = NETWORK_ReadShort() # activator
        data['u_line'] = NETWORK_ReadShort()
        data['u_map'] = NETWORK_ReadString()
        data['u_backSide'] = NETWORK_ReadUByte() != 0
        data['u_arg0'] = NETWORK_ReadLong()
        data['u_arg1'] = NETWORK_ReadLong()
        data['u_arg2'] = NETWORK_ReadLong()
        data['u_always'] = NETWORK_ReadUByte() != 0
    elif lCommand == SVC_SOUND:
        data['u_channel'] = NETWORK_ReadByte()
        data['u_sound'] = NETWORK_ReadString()
        data['u_volume'] = NETWORK_ReadByte()
        if data['u_volume'] > 127:
            data['u_volume'] = 127
        data['u_attenuation'] = NETWORK_ReadByte()
    elif lCommand == SVC_SOUNDACTOR:
        data['u_id'] = NETWORK_ReadShort()
        data['u_channel'] = NETWORK_ReadShort()
        data['u_sound'] = NETWORK_ReadString()
        data['u_volume'] = NETWORK_ReadByte()
        if data['u_volume'] > 127:
            data['u_volume'] = 127
        data['u_attenuation'] = NETWORK_ReadByte()
    elif lCommand == SVC_SOUNDPOINT:
        data['u_x'] = NETWORK_ReadShort() << 16
        data['u_y'] = NETWORK_ReadShort() << 16
        data['u_z'] = NETWORK_ReadShort() << 16
        data['u_channel'] = NETWORK_ReadShort()
        data['u_sound'] = NETWORK_ReadString()
        data['u_volume'] = NETWORK_ReadByte()
        if data['u_volume'] > 127:
            data['u_volume'] = 127
        data['u_attenuation'] = NETWORK_ReadByte()
    elif lCommand == SVC_ANNOUNCERSOUND:
        data['u_sound'] = NETWORK_ReadString()
    elif lCommand == SVC_STARTSECTORSEQUENCE:
        data['u_sector'] = NETWORK_ReadShort()
        data['u_channel'] = NETWORK_ReadByte()
        data['u_sequence'] = NETWORK_ReadString()
        data['u_modenum'] = NETWORK_ReadByte()
    elif lCommand == SVC_STOPSECTORSEQUENCE:
        data['u_sector'] = NETWORK_ReadShort()
    elif lCommand == SVC_CALLVOTE:
        data['u_caller'] = NETWORK_ReadByte()
        data['u_command'] = NETWORK_ReadString()
        data['u_args'] = NETWORK_ReadString()
        data['u_reason'] = NETWORK_ReadString()
    elif lCommand == SVC_PLAYERVOTE:
        data['u_player'] = NETWORK_ReadByte()
        data['u_yes'] = NETWORK_ReadUByte() != 0
    elif lCommand == SVC_VOTEENDED:
        data['u_passed'] = NETWORK_ReadUByte() != 0
    elif lCommand == SVC_MAPLOAD:
        data['u_map'] = NETWORK_ReadString()
    elif lCommand == SVC_MAPNEW:
        data['u_map'] = NETWORK_ReadString()
    elif lCommand == SVC_MAPEXIT:
        data['u_pos'] = NETWORK_ReadByte()
        data['u_nextMap'] = NETWORK_ReadString()
    elif lCommand == SVC_MAPAUTHENTICATE:
        data['u_map'] = NETWORK_ReadString()
    elif lCommand == SVC_SETMAPTIME:
        data['u_time'] = NETWORK_ReadLong()
    elif lCommand == SVC_SETMAPNUMKILLEDMONSTERS:
        data['u_killed_monsters'] = NETWORK_ReadShort()
    elif lCommand == SVC_SETMAPNUMFOUNDITEMS:
        data['u_found_items'] = NETWORK_ReadShort()
    elif lCommand == SVC_SETMAPNUMFOUNDSECRETS:
        data['u_found_secrets'] = NETWORK_ReadShort()
    elif lCommand == SVC_SETMAPNUMTOTALMONSTERS:
        data['u_total_monsters'] = NETWORK_ReadShort()
    elif lCommand == SVC_SETMAPNUMTOTALITEMS:
        data['u_total_items'] = NETWORK_ReadShort()
    elif lCommand == SVC_SETMAPMUSIC:
        data['u_music'] = NETWORK_ReadString()
    elif lCommand == SVC_SETMAPSKY:
        data['u_sky1'] = NETWORK_ReadString()
        data['u_sky2'] = NETWORK_ReadString()
    elif lCommand == SVC_GIVEINVENTORY:
        data['u_player'] = NETWORK_ReadByte()
        data['u_actorNetworkIndex'] = NETWORK_ReadShort()
        data['u_amount'] = NETWORK_ReadShort()
    elif lCommand == SVC_TAKEINVENTORY:
        data['u_player'] = NETWORK_ReadByte()
        data['u_item'] = NETWORK_ReadString()
        data['u_amount'] = NETWORK_ReadShort()
    elif lCommand == SVC_GIVEPOWERUP:
        data['u_player'] = NETWORK_ReadByte()
        data['u_actorNetworkIndex'] = NETWORK_ReadShort()
        data['u_amount'] = NETWORK_ReadShort()
        data['u_effectTics'] = NETWORK_ReadShort()
    elif lCommand == SVC_DOINVENTORYPICKUP:
        data['u_player'] = NETWORK_ReadByte()
        data['u_item'] = NETWORK_ReadString()
        data['u_message'] = NETWORK_ReadString()
    elif lCommand == SVC_DESTROYALLINVENTORY:
        data['u_player'] = NETWORK_ReadByte()
    elif lCommand == SVC_DODOOR:
        data['u_sector'] = NETWORK_ReadShort()
        data['u_speed'] = NETWORK_ReadLong()
        data['u_direction'] = NETWORK_ReadByte()
        data['u_lightTag'] = NETWORK_ReadShort()
        data['u_doorId'] = NETWORK_ReadShort()
    elif lCommand == SVC_DESTROYDOOR:
        data['u_door'] = NETWORK_ReadShort()
    elif lCommand == SVC_CHANGEDOORDIRECTION:
        data['u_door'] = NETWORK_ReadShort()
        data['u_direction'] = NETWORK_ReadByte()
    elif lCommand == SVC_DOFLOOR:
        data['u_type'] = NETWORK_ReadByte()
        data['u_sector'] = NETWORK_ReadShort()
        data['u_direction'] = NETWORK_ReadByte()
        data['u_speed'] = NETWORK_ReadLong()
        data['u_floorDestDist'] = NETWORK_ReadLong()
        data['u_floor'] = NETWORK_ReadShort()
    elif lCommand == SVC_DESTROYFLOOR:
        data['u_floor'] = NETWORK_ReadShort()
    elif lCommand == SVC_CHANGEFLOORDIRECTION:
        data['u_floor'] = NETWORK_ReadShort()
        data['u_direction'] = NETWORK_ReadByte()
    elif lCommand == SVC_CHANGEFLOORTYPE:
        data['u_floor'] = NETWORK_ReadShort()
        data['u_type'] = NETWORK_ReadByte()
    elif lCommand == SVC_CHANGEFLOORDESTDIST:
        data['u_floor'] = NETWORK_ReadShort()
        data['u_destDist'] = NETWORK_ReadLong()
    elif lCommand == SVC_STARTFLOORSOUND:
        data['u_floor'] = NETWORK_ReadShort()
    elif lCommand == SVC_DOCEILING:
        data['u_type'] = NETWORK_ReadByte()
        data['u_sector'] = NETWORK_ReadShort()
        data['u_direction'] = NETWORK_ReadByte()
        data['u_bottomHeight'] = NETWORK_ReadLong()
        data['u_topHeight'] = NETWORK_ReadLong()
        data['u_speed'] = NETWORK_ReadLong()
        data['u_crush'] = NETWORK_ReadShort()
        data['u_silent'] = NETWORK_ReadShort()
        data['u_ceiling'] = NETWORK_ReadShort()
    elif lCommand == SVC_DESTROYCEILING:
        data['u_ceiling'] = NETWORK_ReadShort()
    elif lCommand == SVC_CHANGECEILINGDIRECTION:
        data['u_ceiling'] = NETWORK_ReadShort()
        data['u_direction'] = NETWORK_ReadByte()
    elif lCommand == SVC_CHANGECEILINGSPEED:
        data['u_ceiling'] = NETWORK_ReadShort()
        data['u_speed'] = NETWORK_ReadLong()
    elif lCommand == SVC_PLAYCEILINGSOUND:
        data['u_ceiling'] = NETWORK_ReadShort()
    elif lCommand == SVC_DOPLAT:
        data['u_type'] = NETWORK_ReadByte()
        data['u_sector'] = NETWORK_ReadShort()
        data['u_status'] = NETWORK_ReadByte()
        data['u_high'] = NETWORK_ReadLong()
        data['u_low'] = NETWORK_ReadLong()
        data['u_speed'] = NETWORK_ReadLong()
        data['u_plat'] = NETWORK_ReadShort()
    elif lCommand == SVC_DESTROYPLAT:
        data['u_plat'] = NETWORK_ReadShort()
    elif lCommand == SVC_CHANGEPLATSTATUS:
        data['u_plat'] = NETWORK_ReadShort()
        data['u_status'] = NETWORK_ReadByte()
    elif lCommand == SVC_PLAYPLATSOUND:
        data['u_plat'] = NETWORK_ReadShort()
        data['u_soundType'] = NETWORK_ReadByte()
    elif lCommand == SVC_DOELEVATOR:
        data['u_type'] = NETWORK_ReadByte()
        data['u_sector'] = NETWORK_ReadLong()
        data['u_direction'] = NETWORK_ReadByte()
        data['u_floorDestDist'] = NETWORK_ReadLong()
        data['u_ceilingDestDist'] = NETWORK_ReadLong()
        data['u_elevator'] = NETWORK_ReadShort()
    elif lCommand == SVC_DESTROYELEVATOR:
        data['u_elevator'] = NETWORK_ReadShort()
    elif lCommand == SVC_STARTELEVATORSOUND:
        data['u_elevator'] = NETWORK_ReadShort()
    elif lCommand == SVC_DOPILLAR:
        data['u_type'] = NETWORK_ReadByte()
        data['u_sector'] = NETWORK_ReadShort()
        data['u_floorSpeed'] = NETWORK_ReadLong()
        data['u_ceilingSpeed'] = NETWORK_ReadLong()
        data['u_floorTarget'] = NETWORK_ReadLong()
        data['u_ceilingTarget'] = NETWORK_ReadLong()
        data['u_pillar'] = NETWORK_ReadShort()
    elif lCommand == SVC_DESTROYPILLAR:
        data['u_pillar'] = NETWORK_ReadShort()
    elif lCommand == SVC_DOWAGGLE:
        data['u_ceiling'] = NETWORK_ReadUByte != 0
        data['u_sector'] = NETWORK_ReadShort()
        data['u_originalDistance'] = NETWORK_ReadLong()
        data['u_accumulator'] = NETWORK_ReadLong()
        data['u_accelerationDelta'] = NETWORK_ReadLong()
        data['u_targetScale'] = NETWORK_ReadLong()
        data['u_scale'] = NETWORK_ReadLong()
        data['u_scaleDelta'] = NETWORK_ReadLong()
        data['u_ticker'] = NETWORK_ReadLong()
        data['u_state'] = NETWORK_ReadByte()
        data['u_waggle'] = NETWORK_ReadShort()
    elif lCommand == SVC_DESTROYWAGGLE:
        data['u_waggle'] = NETWORK_ReadShort()
    elif lCommand == SVC_UPDATEWAGGLE:
        data['u_waggle'] = NETWORK_ReadShort()
        data['u_accumulator'] = NETWORK_ReadLong()
    elif lCommand == SVC_DOROTATEPOLY:
        data['u_speed'] = NETWORK_ReadLong()
        data['u_poly'] = NETWORK_ReadShort()
    elif lCommand == SVC_DESTROYROTATEPOLY:
        data['u_rotator_id'] = NETWORK_ReadShort()
    elif lCommand == SVC_DOMOVEPOLY:
        data['u_speedX'] = NETWORK_ReadLong()
        data['u_speedY'] = NETWORK_ReadLong()
        data['u_poly'] = NETWORK_ReadShort()
    elif lCommand == SVC_DESTROYMOVEPOLY:
        data['u_mover_id'] = NETWORK_ReadShort()
    elif lCommand == SVC_DOPOLYDOOR:
        data['u_type'] = NETWORK_ReadByte()
        data['u_speedX'] = NETWORK_ReadLong()
        data['u_speedY'] = NETWORK_ReadLong()
        data['u_speed'] = NETWORK_ReadLong()
        data['u_poly'] = NETWORK_ReadShort()
    elif lCommand == SVC_DESTROYPOLYDOOR:
        data['u_door_id'] = NETWORK_ReadShort()
    elif lCommand == SVC_SETPOLYDOORSPEEDPOSITION:
        data['u_poly'] = NETWORK_ReadShort()
        data['u_speedX'] = NETWORK_ReadLong()
        data['u_speedY'] = NETWORK_ReadLong()
    elif lCommand == SVC_SETPOLYDOORSPEEDROTATION:
        data['u_poly'] = NETWORK_ReadShort()
        data['u_speed'] = NETWORK_ReadLong()
        data['u_angle'] = NETWORK_ReadLong()
    elif lCommand == SVC_PLAYPOLYOBJSOUND:
        data['u_poly'] = NETWORK_ReadShort()
        data['u_soundId'] = NETWORK_ReadByte()
    elif lCommand == SVC_SETPOLYOBJPOSITION:
        data['u_poly'] = NETWORK_ReadShort()
        data['u_x'] = NETWORK_ReadLong()
        data['u_y'] = NETWORK_ReadLong()
    elif lCommand == SVC_SETPOLYOBJROTATION:
        data['u_poly'] = NETWORK_ReadShort()
        data['u_angle'] = NETWORK_ReadLong()
    elif lCommand == SVC_EARTHQUAKE:
        data['u_center'] = NETWORK_ReadShort()
        data['u_intensity'] = NETWORK_ReadByte()
        data['u_duration'] = NETWORK_ReadByte()
        data['u_tremrad'] = NETWORK_ReadByte()
    elif lCommand == SVC_SETQUEUEPOSITION:
        data['u_position'] = NETWORK_ReadByte()
    elif lCommand == SVC_DOSCROLLER:
        data['u_type'] = NETWORK_ReadByte()
        data['u_dX'] = NETWORK_ReadLong()
        data['u_dY'] = NETWORK_ReadLong()
        data['u_affectee'] = NETWORK_ReadShort()
    elif lCommand == SVC_SETSCROLLER:
        data['u_type'] == NETWORK_ReadByte()
        data['u_dX'] = NETWORK_ReadLong()
        data['u_dY'] = NETWORK_ReadLong()
        data['u_tag'] = NETWORK_ReadShort()
    elif lCommand == SVC_SETWALLSCROLLER:
        data['u_id'] = NETWORK_ReadLong()
        data['u_side'] = NETWORK_ReadByte()
        data['u_dX'] = NETWORK_ReadLong()
        data['u_dY'] = NETWORK_ReadLong()
        data['u_where'] = NETWORK_ReadLong()
    elif lCommand == SVC_DOFLASHFADER:
        data['u_fR1'] = NETWORK_ReadFloat()
        data['u_fG1'] = NETWORK_ReadFloat()
        data['u_fB1'] = NETWORK_ReadFloat()
        data['u_fA1'] = NETWORK_ReadFloat()
        data['u_fR2'] = NETWORK_ReadFloat()
        data['u_fG2'] = NETWORK_ReadFloat()
        data['u_fB2'] = NETWORK_ReadFloat()
        data['u_fA2'] = NETWORK_ReadFloat()
        data['u_time'] = NETWORK_ReadFloat()
        data['u_player'] = NETWORK_ReadByte()
    elif lCommand == SVC_GENERICCHEAT:
        data['u_player'] = NETWORK_ReadByte()
        data['u_cheat'] = NETWORK_ReadByte()
    elif lCommand == SVC_SETCAMERATOTEXTURE:
        data['u_camera'] = NETWORK_ReadShort()
        data['u_texture'] = NETWORK_ReadString()
        data['u_fov'] = NETWORK_ReadByte()
    elif lCommand == SVC_CREATETRANSLATION or lCommand == SVC_CREATETRANSLATION2:
        data['u_idx'] = NETWORK_ReadShort()
        data['u_edited'] = NETWORK_ReadUByte() != 0
        data['u_start'] = NETWORK_ReadByte()
        data['u_end'] = NETWORK_ReadByte()
        if lCommand == SVC_CREATETRANSLATION2:
            data['u_ulR1'] = NETWORK_ReadUByte()
            data['u_ulG1'] = NETWORK_ReadUByte()
            data['u_ulB1'] = NETWORK_ReadUByte()
            data['u_ulR2'] = NETWORK_ReadUByte()
            data['u_ulG2'] = NETWORK_ReadUByte()
            data['u_ulB2'] = NETWORK_ReadUByte()
        else:
            data['u_pal1'] = NETWORK_ReadByte()
            data['u_pal2'] = NETWORK_ReadByte()
    elif lCommand == SVC_IGNOREPLAYER:
        data['u_player'] = NETWORK_ReadByte()
        data['u_ticks'] = NETWORK_ReadLong()
    elif lCommand == SVC_DOPUSHER:
        data['u_type'] = NETWORK_ReadByte()
        data['u_line'] = NETWORK_ReadShort()
        data['u_magnitude'] = NETWORK_ReadLong()
        data['u_angle'] = NETWORK_ReadLong()
        data['u_sourceNetId'] = NETWORK_ReadShort()
        data['u_affectee'] = NETWORK_ReadShort()
    elif lCommand == SVC_EXTENDEDCOMMAND:
        lExtCommand = NETWORK_ReadUByte()
        #print('lExtCommand = %02X (%u)' % (lExtCommand, lExtCommand))
        if lExtCommand == SVC2_SETINVENTORYICON:
            data['u_player'] = NETWORK_ReadByte()
            data['u_actorNetworkIndex'] = NETWORK_ReadShort()
            data['u_icon'] = NETWORK_ReadString()
        elif lExtCommand == SVC2_FULLUPDATECOMPLETED:
            pass
        elif lExtCommand == SVC2_SETIGNOREWEAPONSELECT:
            data['u_ignore'] = NETWORK_ReadUByte() != 0
        elif lExtCommand == SVC2_CLEARCONSOLEPLAYERWEAPON:
            pass
        elif lExtCommand == SVC2_LIGHTNING:
            pass
        elif lExtCommand == SVC2_CANCELFADE:
            data['u_player'] = NETWORK_ReadByte()
        elif lExtCommand == SVC2_PLAYBOUNCESOUND:
            data['u_actorNetworkIndex'] = NETWORK_ReadShort()
            data['u_onFloor'] = NETWORK_ReadUByte() != 0
        elif lExtCommand == SVC2_GIVEWEAPONHOLDER:
            data['u_player'] = NETWORK_ReadByte()
            data['u_pieceMask'] = NETWORK_ReadUShort()
            data['u_actorNetworkIndex'] = NETWORK_ReadShort()
        elif lExtCommand == SVC2_SETHEXENARMORSLOTS:
            data['u_player'] = NETWORK_ReadByte()
            data['u_armorSlots'] = []
            for i in range(5):
                data['u_armorSlots'].append(NETWORK_ReadLong())
        elif lExtCommand == SVC2_SETTHINGREACTIONTIME:
            data['u_actorNetworkIndex'] = NETWORK_ReadShort()
            data['u_reactionTime'] = NETWORK_ReadShort()
        elif lExtCommand == SVC2_SETFASTCHASESTRAFECOUNT:
            data['u_actorNetworkIndex'] = NETWORK_ReadShort()
            data['u_strafeCount'] = NETWORK_ReadByte()
        elif lExtCommand == SVC2_RESETMAP:
            pass
        elif lExtCommand == SVC2_SETPOWERUPBLENDCOLOR:
            data['u_player'] = NETWORK_ReadByte()
            data['u_actorNetworkIndex'] = NETWORK_ReadShort()
            data['u_blendColor'] = NETWORK_ReadULong()
        elif lExtCommand == SVC2_SETPLAYERHAZARDCOUNT:
            data['u_player'] = NETWORK_ReadByte()
            data['u_hz'] = NETWORK_ReadShort()
        elif lExtCommand == SVC2_SCROLL3DMIDTEX:
            data['u_i'] = NETWORK_ReadByte()
            data['u_move'] = NETWORK_ReadLong()
            data['u_isCeiling'] = NETWORK_ReadUByte() != 0
        elif lExtCommand == SVC2_SETPLAYERLOGNUMBER:
            data['u_player'] = NETWORK_ReadByte()
            data['u_arg0'] = NETWORK_ReadShort()
        elif lExtCommand == SVC2_SETTHINGSPECIAL:
            data['u_actorNetworkIndex'] = NETWORK_ReadShort()
            data['u_special'] = NETWORK_ReadShort()
        elif lExtCommand == SVC2_SYNCPATHFOLLOWER:
            pass
        else:
            print('Erroneous extended command (%02X, %u).' % (lExtCommand, lExtCommand))
            break
    else:
        print('Erroneous command (%02X, %u).' % (lCommand, lCommand))
        break
    """
    # if we made it to here, it means that we have a complete packet that we can send
    # look for SVC_ string for lCommand (and for SVC2_ string for lExtCommand, if present)
    lCommandStr = 'SVC_UNKNOWN'
    lExtCommandStr = 'SVC2_UNKNOWN' if lCommand == SVC_EXTENDEDCOMMAND else ''
    for ent in cldenum.__dict__:
        if (ent.find('SVC_') == 0 or ent.find('CLD_') == 0 or ent.find('SVCC_') == 0) and cldenum.__dict__[ent] == lCommand:
            lCommandStr = ent
        if lCommand == SVC_EXTENDEDCOMMAND and ent.find('SVC2_') == 0 and cldenum.__dict__[ent] == lExtCommand:
            lExtCommandStr = ent
    cldproc.proc(lCommand, lExtCommand, lCommandStr, lExtCommandStr, data)
    """
    cldproc.proc(lCommand, lExtCommand, data)
