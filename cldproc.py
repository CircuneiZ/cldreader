# -*- coding: utf-8 -*-

import console
import copy
from cldenum import *

Players = {}
Teams = {}

def CleanString(string):
    outstr = ''
    i = 0
    color_complex = False
    while i < len(string):
        if string[i] == '\\' and string[i+1] == 'n':
            outstr += '\n'
            i += 2
            continue
        crep1 = (string[i] == '\\' and string[i+1] == 'c')
        if crep1:
            i += 1
        if string[i] == '\034' or crep1:
            # skip the color
            if string[i+1] == '[':
                color_complex = True
                i += 1
                continue
            else:
                color_complex = False
                i += 2
                continue
        elif color_complex:
            if string[i] == ']':
                color_complex = False
            i += 1
            continue
        outstr += string[i]
        i += 1
    return outstr

(t_w, t_h) = console.getTerminalSize()
    
def proc(command, ext_command, data):
    global Players
    if command == SVCC_AUTHENTICATE or command == SVC_MAPLOAD:
        print('')
        print('='*(t_w-1))
        print(' SWITCHED TO MAP: %s' % (data['u_map'].upper()))
        print('='*(t_w-1))
        print('')
    elif command == SVC_SETPLAYERUSERINFO:
        Players[data['u_player']] = data
        #print(repr(data))
    elif command == SVC_PLAYERSAY:
        _mode = ''
        if data['u_mode'] == 2:
            _mode = '<TEAM> '
        elif data['u_mode'] == 3:
            _mode = '<SPEC> '
        _name = CleanString(Players[data['u_player']]['u_name'])
        _message = CleanString(data['u_string'])
        if _message.find("/me") == 0:
            print('%s* %s%s' % (_mode, _name, _message[3:]))
        else:
            print('%-26s %s' % (_mode+_name+':', _message))
        #print('%d: %s' % (data['u_player'], CleanString(data['u_string'])))
        pass
    #if command == SVC_PLAYERSAY:
    #    print(repr(data))
